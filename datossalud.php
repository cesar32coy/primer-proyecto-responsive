<?php 
	include("conexion/conectar.php");
	
   ?>  

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.dataTables.min.css">
   
    <!-- <link rel="stylesheet" type="text/css" href="css/miestilo.css"> -->
    <link rel="stylesheet" type="text/css" href="css/strength.css">
    <link rel="stylesheet" href="bootstrap-dist/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    	<link rel="icon" href="../imagen/logooficial.jpeg" width="30" height="30" class="d-inline-block 
    align-top">
	
    
	<title> Salud</title>
</head>
<body>


    <h3 class="center">Datos Salud Animal</h3>
    
    <div class="row">
      <a id="mostrar" class="btn btn-trigger" href="#mdlpp">Agregar Registro</a>
      <a id="mostrar" class="btn btn-trigger" href="vacuna.php">Vacunas</a>
      <a id="mostrar" class="btn btn-trigger" href="vitaminas.php">Vitaminas</a>
      <a id="mostrar" class="btn btn-trigger" href="reportesalud.php">PDF</a>
    </div>
    <div class="contenedortabla text-center">
    
    
    <br>
    <div class="row lighten-2">
    <div class="col s12  ">
      <!-- Se usa una tabla con las funciones de DataTable -->
      <table id="example" class="cell-border compact stripe" style="width:100%">
        <thead class="black">
            <tr>
                <th >Id</th>
                <th class="white-text">Nombre</th>
                <th class="white-text">Apellidos</th>
                <th class="white-text">Cumpleaños</th>
                <th class="white-text">Direccion</th>
                <th class="white-text">editar</th>
                <th class="white-text">eliminar</th>
                
            </tr>
        </thead>
        <tbody>
          <?php
                /* Se llama a uno campos de la tablas saludanimal y a traves de esta se usa inner join para llamar las columnas relacionadas a animal */
                $sqlcon='SELECT * FROM fechacumpleanos';
                $result=mysqli_query($conectar,$sqlcon);
                while($row=mysqli_fetch_array($result)){
                  /* Se rellenan las variables segun lo traido por la query a mysqli */
                    $id=$row['id'];
                    $nombre=$row['nombre'];
                    $Apellidos= $row['apellidos'];
                    $fecha= $row['fecha'];
                    $direccion= $row['direccion'];
                    
             ?>
             <tr>
              <!-- Se pone las variables en las respectivas td -->
                
                <td><?php echo $id?></td>
                <td><?php echo $nombre?></td>
                <td><?php echo $Apellidos?></td>
                <td><?php echo $fecha?></td>
                <td><?php echo $direccion?></td>
               
                <!-- Se producen eventos con los button que llaman a una fila completa las cuales se usan para actualizar o eliminar -->
                <td>
                 <button id="up" class="btn btn-trigger edit" href="#mdup" value="<?php echo $IdSalud ?>"><i class="material-icons">edit</i></button>
                 </td>
                 <td>
                  <button id="del" class="btn"><i class="material-icons red-text">delete</i></button>
                  </td>
              </tr>
              <?php 
                } 
            ?>

        </tbody>
    </table>
    </div>  
  </div>
  </div>



  <!-- Se usa modal para poder acutalizar el cual se autocompleta segun lo traido por la DataTable usando ajax-->
  <div class="modal modal-fixed-footer" id="mdlpp">
    <div class="modal-content">
        <h5 class="modal-title">Agregar Datos </h5>
          <form id="frm">
            <div class="row">
            <div class="input-field col s12">
                <i class="material-icons prefix">user</i>
              <input type="text" id="nombre" name="nombre" placeholder="nombre" class="validate" >
              <label for="nombre" >nombre</label>
            </div>
              <div class="input-field col s12">
                <i class="material-icons prefix">user</i>
              <input type="text" id="apellidos" name="apellidos" placeholder="apellidos" class="validate" >
              <label for="apellidos" >apellidos</label>
            </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                <i class="material-icons prefix">user</i>
              <input type="text" id="direccion" name="direccion" placeholder="direccion" class="validate" >
              <label for="direccion" >direccion</label>
            </div>
              </div>
              
            <div class="row">
                <div class="input-field col s12">
                  <p>Fecha</p>
                  <input type="date" id="fecha" required>
                </div>
                
              </div>
              <br>
              <div  id="resultado2">
              </div>
        
            </div>  
          <div class="modal-footer">
             <a href="datossalud.php" class="modal-action modal-close btn red"><span>Cancelar</span></a>
            <button class="btn btn-success" id="submit" type="submit">Registrar</button>
          </div>
          </form>
      </div>
  <div class="modal modal-fixed-footer" id="mdup">
     <form id="frmup">
    <div class="modal-content">
        <h5 class="modal-title">Actualizar Datos Salud</h5>
            <div class="row">
              <div class="input-field col s12">
                <p>Numero registro:</p>
                <p id="salud">  </p>
              </div>
              <br>
              <div class="input-field col s12">
                <p>Nombre Animal:</p>
                <p id="nombre1"></p>
              </div>
              </div>
              <br>
              <div class="row">
                <div class="input-field col s12">
                 <select id="medicacion1" required>
                    <option selected>Selecciona medicacion</option>
                    <option value="Enfermedad">Enfermedad</option>    
                    <option value="Accidente">Accidente</option>
                    <option value="Otros">Otros</option>
                 </select>
            </div>
              </div>
              <div class="row">
    <label for="textarea">Tratamiento</label>
    <textarea class="materialize-textarea" id="tratamiento1" rows="3" required></textarea>
              </div>
            <div class="row">
                <div class="input-field col s12">
                  <p>Fecha Inicio</p>
                  <h6 id="inicio1"></h6>
                </div>
                <div class="input-field col s12">
                  <p>Fecha Finalizacion</p>
                  <input type="date" id="fin1" required>
                </div>
              </div>
              <br>
              <div class="row" id="resultadoup">
              </div>
            </div>
          <div class="modal-footer">
            <a href="datossalud.php" class="modal-action modal-close btn red"><span>Cancelar</span></a>
            <button class="btn btn-success" id="submit" type="submit">Actualizar</button>
          </div>
          </form>
    </div>
<div class="modal modal-fixed-footer" id="mddel">
  <form enctype="multipar/form-data" id="frmdel">
    <div class="modal-content">
        <h5 class="modal-title">Eliminar</h5>
        <div class="card-header"><h5 class="card-title">Datos Salud</h5></div><br>
            <div class="row">
              <div class="input-field col s12">
                <label style="display: none;" id="idsalud"></label>
              </div>
            </div>
            <div class="input-field col s12 red-text">
              <p> esta seguro que desea eliminarlo</p>
              </div>
              <div class="row" id="resultado1">
              </div>
            </div>
          <div class="modal-footer">
             <a href="datossalud.php" class="modal-action modal-close btn red"><span>Cancelar</span></a>
            <button class="btn btn-success" id="submit" type="submit">Eliminar</button>
          </div>
          </form>
</div>
	

  <?php include("scripts.php"); ?>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/datepicker.js"></script>
<script type="text/javascript" src="js/validacion.js"></script>
<script src="bootstrap-dist/js/datossalud.js"> 
</script>
</body>
</html>