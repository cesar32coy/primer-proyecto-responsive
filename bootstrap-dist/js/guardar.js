$(document).ready(function(){
  $("#frm").submit(function(e){
        e.preventDefault();
        
        var nombre = $("#nombre").val(); 
        var apellidos = $("#apellidos").val();
        var direccion = $("#direccion").val();
        var fecha = $("#fecha").val();

        if(nombre == ''){
          $('#lbNombre').html("<span style='color:red;'> complete el campo nombre</span>");
          $('#nombre').focus();
             return false;
        }else if (apellidos == '') {
          $('#lbApellidos').html("<span style='color:red;'> complete el campo apellido</span>");
          $('#apellidos').focus();
             return false;
        }else if (fecha == '') {
          $('#lbFecha').html("<span style='color:red;'> complete el campo fecha</span>");
          $('#fecha').focus();
             return false;
        }else if (direccion == '') {
          $('#lbDireccion').html("<span style='color:red;'> complete el campo direccion</span>");
          $('#direccion').focus();
             return false;
        }  
       
        $.ajax({
          url:"registroUser.php",
          method:"POST",
          data:{"nombre":nombre,"apellidos":apellidos,"fecha":fecha,"direccion":direccion},
         
          success: function(data){
            setTimeout(function(){
            
            $("#exampleModal").modal('hide');
            $("#resultado2").html("<img src='https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif' width='100px'/>");
            location.reload();
            }, 2000);
            
            }

      });
      }); 

});
