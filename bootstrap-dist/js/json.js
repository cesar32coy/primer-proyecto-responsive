$(document).ready(function(){

 var table = $('#example').DataTable({

         
          "destroy":true,

          language: {
    "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
},

        "ajax":{
          "url":"ajax.php",
          "method":"GET",
          
        },
         scrollY:        "400px",
        scrollX:        true,
        scrollCollapse: true,
       
        fixedColumns:   {
           rightColumns: 1
        },
        responsive:false,
         order: [0, 'desc'],
          "columns":[
                
                {"data":"id",visible:false},
                {"data":"nombre"},
                {"data":"apellidos"},
                {"data":"cumpleanos"},
                {"data":"direccion"},
                {"data":"ciudad"},
                {"defaultContent": "<button type='button' class='btn-del btn btn-danger' data-toggle='modal' data-target='#eliminar' data-whatever='@mdo'><i class='far fa-trash-alt'></i></button><button type='button' class='editar btn btn-warning' data-toggle='modal' data-target='#editar' data-whatever='@mdo'><i class='far fa-edit'></i></button>"},
               


            ]


        });

 setInterval( function () {
    table.ajax.reload( null, false ); // user paging is not reset on reload
}, 5000 );
 

  editar("#example tbody", table);
  eliminar("#example tbody", table);

  });

    
    $("#nombre").change(function(){
      if ($("#nombre").val() != '') {
          $("#lbNombre").html("");
      }else{
          $('#lbNombre').html("<span style='color:red;'> complete el campo nombre</span>");

      }
    });

     $("#apellidos").change(function(){
      if ($("#apellidos").val() != '') {
          $("#lbApellidos").html("");
      }else{
          $('#lbApellidos').html("<span style='color:red;'> complete el campo Apellidos</span>");

      }
    });
    $("#fecha").change(function(){
      if ($("#fecha").val() != '') {
          $("#lbFecha").html("");
      }else{
          $('#lbFecha').html("<span style='color:red;'> complete el campo Fecha</span>");

      }
    });


    $("#direccion").change(function(){
      if ($("#direccion").val() != '') {
          $("#lbDireccion").html("");
      }else{
          $('#lbDireccion').html("<span style='color:red;'> complete el campo Direccion</span>");

      }
    });
 
 

     
  $("#frm").submit(function(e){
        e.preventDefault();
       var fun1 = "funregistrar";
       var funconsulta = "funconsulta";
        var nombre = $("#nombre").val(); 
        var apellidos = $("#apellidos").val();
        var direccion = $("#direccion").val();
        var fecha = $("#fecha").val();
        var ciudad = $("#ciudad").val();

        $.ajax({
          url:"registroUser.php",
          method:"POST",
          data:{"funcion":funconsulta,"nombre":nombre},
         beforeSend:function(){
            $("#preload").html("<img src='imagen/22512-loading-animation.gif' width='120' heigth='120'>");
         },
          success: function(data){
           var nombres = data.map(function(nombre){
              console.log(nombre)
              
           })
             
              
             setTimeout(function() {
                  $("#resultado2").html(data).fadeOut(3000);
             },3000);
            
            $("#frm").trigger('reset');
            if (data) {
              $("#preload").fadeOut(3000);
              $("#exampleModal").modal("hide");
               setTimeout(function() {
                  $("body").load("tableResponsive.php");
             },500);
              
            }
 
            }

        });

        if(nombre == ''){
          
           $('#lbNombre').html("<span style='color:red;'> complete el campo nombre</span>");
            
          $('#nombre').focus();
             return false;
        }else if (apellidos == '') {
          
          $('#lbApellidos').html("<span style='color:red;'> complete el campo apellido</span>");
          $('#apellidos').focus();
             return false;
        }else if (fecha == '') {
          
           $('#lbFecha').html("<span style='color:red;'> complete el campo fecha</span>");
           $('#fecha').focus();
             return false;

        }else if (direccion == '') {
           
            $('#lbDireccion').html("<span style='color:red;'> complete el campo direccion</span>");
            $('#direccion').focus();
             return false;
        }else{
          $.ajax({
          url:"registroUser.php",
          method:"POST",

          data:{"funcion":fun1,"nombre":nombre,"apellidos":apellidos,"fecha":fecha,"direccion":direccion,"ciudad":ciudad},
         beforeSend:function(){
            $("#preload").html("<img src='imagen/22512-loading-animation.gif' width='120' heigth='120'>");
         },
          success: function(data){
              console.log(data);
             setTimeout(function() {
                  $("#resultado2").html(data).fadeOut(3000);
             },3000);
            
            $("#frm").trigger('reset');
            if (data) {
              $("#preload").fadeOut(3000);
              $("#exampleModal").modal("hide");
               setTimeout(function() {
                  $("body").load("tableResponsive.php");
             },500);
              
            }
 
            }

        });
        }
   
      }); 

  		
 
 
 var editar = function(tbody, table){
        	$(tbody).on("click", "button.editar",function(){
        		var data = table.row($(this).parents("tr")).data();
        		var id = $("#id").val(data.id),
	        		nombre = $("#nombre2").val(data.nombre),
	        		apellidos = $("#apellidos2").val(data.apellidos),
	        		cumpleanos = $("#fecha2").val(data.cumpleanos),
	        		direccion = $("#direccion2").val(data.direccion);
	        	
        	});
             $("#frmedit").submit(function(e){
        e.preventDefault();
        var fun = "funedit";
        var id = $("#id").val(); 
       var nombre = $("#nombre2").val(); 
        var apellidos = $("#apellidos2").val();
        var direccion = $("#direccion2").val();
        var fecha = $("#fecha2").val();
        $.ajax({
          url:"registroUser.php",
          method:"POST",
          data:{"funcion":fun,"id":id,"nombre":nombre,"apellidos":apellidos,"direccion":direccion},
           
          success: function(data){
           
            $("#editar").modal('hide');

             setTimeout(function() {
                 $("#resultadoedit").html(data).fadeOut(3000);
             },2000);

              setTimeout(function() {
                  $("body").load("tableResponsive.php");
             },500);
              
             
            }


      });

      }); 
        }
      

         var eliminar = function(tbody, table){
        	$(tbody).on("click", "button.btn-del",function(){
        		var data = table.row($(this).parents("tr")).data();
        		var id = $("#ids").val(data.id);
				
        	});
        }
        $("#frmeliminar").submit(function(e){
        e.preventDefault();
        var fun = "funelim";
        var id = $("#ids").val(); 
  		 
        $.ajax({
			          url:"registroUser.php",
			          method:"POST",
			          data:{"funcion":fun,"id":id},
			         	
			          success: function(data){
                   $("#eliminar").modal('hide');
                    setTimeout(function() {
                  $("#resultadoelim").html(data).fadeOut(3000);
             },2000);
               setTimeout(function() {
                  $("body").load("tableResponsive.php");
             },500);
              
	          
            }

      });
      }); 
       
                    
    
