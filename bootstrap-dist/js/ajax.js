$(document).ready(function(){
  datos();
  table();
  });
  
  $("#frm").submit(function(e){
        e.preventDefault();
       
        var nombre = $("#nombre").val(); 
        var apellidos = $("#apellidos").val();
        var direccion = $("#direccion").val();
        var fecha = $("#fecha").val();

        if(nombre == ''){
          $('#lbNombre').html("<span style='color:red;'> complete el campo nombre</span>");
          $('#nombre').focus();
             return false;
        }else if (apellidos == '') {
          $('#lbApellidos').html("<span style='color:red;'> complete el campo apellido</span>");
          $('#apellidos').focus();
             return false;
        }else if (fecha == '') {
          $('#lbFecha').html("<span style='color:red;'> complete el campo fecha</span>");
          $('#fecha').focus();
             return false;
        }else if (direccion == '') {
          $('#lbDireccion').html("<span style='color:red;'> complete el campo direccion</span>");
          $('#direccion').focus();
             return false;
        }  
       
        $.ajax({
          url:"registroUser.php",
          method:"POST",
          data:{"nombre":nombre,"apellidos":apellidos,"fecha":fecha,"direccion":direccion},
         
          success: function(data){
           
            datos();
            $("#exampleModal").modal('hide');
            $("#resultado2").html(data);
            $("#frm").trigger('reset');

            
            }

      });
      }); 

    function datos(){
        $.ajax({
              url:"datos.php",
              method:"GET",

              success: function(response){
               let datos = JSON.parse(response);
               let tabla = '';
               datos.forEach(dato =>{
                tabla += `<tr Id="${dato.id}">
                          <td>${dato.id}</td>
                          <td>${dato.nombre}</td>
                          <td>${dato.apellidos}</td>
                          <td>${dato.fecha}</td>
                          <td>${dato.direccion}</td>
                          <td><button class="btn-del btn btn-danger">Eliminar</button></td>
                          </tr>`
               });

                $('#dato').html(tabla);
    
                }
          }); 
        }

        function table(){
        var table = $('#example').DataTable({
          language: {
        processing:     "Traitement en cours...",
        search:         "Buscar:",
        lengthMenu:    "Mostrar _MENU_ entradas",
        info:           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
        infoEmpty:      "Mostrando  0 a 0 de 0 elementos",
        infoFiltered:   "(de _MAX_ en total)",
        infoPostFix:    "",
        loadingRecords: "Cargando registros",
        zeroRecords:    "No hay registro",
        emptyTable:     "No se muestran datos",
        paginate: {
            first:      "Primero",
            previous:   "Previo",
            next:       "Siguente",
            last:       "Ultimo"
          },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
          }
        },
          responsive: true

        });

        $(document).on('click', '.btn-del', function(){
           Swal.fire({
                      title: 'Estas seguro que deseas Eliminar?',
                      text: 'Al Eliminarlo no Lo podras recuperar!',
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Si, Eliminarlo!'
                    }).then((result) => {
                      if (result.value) {
                        
                     let elemento = $(this)[0].parentElement.parentElement;
                    let id = $(elemento).attr('Id');
                    $.post('eliminar_u.php', {id}, function(response){
                       
                    });
                        datos();                
                      }
                      });
         
         });
  }

