<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="bootstrap-dist/css/jquery.dataTables.min.css">
	
    
    <link rel="stylesheet" href="bootstrap-dist/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="bootstrap-dist/css/select.dataTables.min.css">
    <link rel="stylesheet" href="bootstrap-dist/css/editor.dataTables.min.css">
    <link rel="stylesheet" href="bootstrap-dist/css/style.css">
    <link href="/your-path-to-fontawesome/css/all.css" rel="stylesheet">
	<title>TABLE EDITOR</title>
</head>
<body>
	<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Extn.</th>
                <th>Start date</th>
                <th>Salary</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Extn.</th>
                <th>Start date</th>
                <th>Salary</th>
            </tr>
        </tfoot>
    </table>
    <script type="text/javascript" src="bootstrap-dist/js/jquery-3.3.1.js"></script>
    
	<script type="text/javascript" src="bootstrap-dist/js/jquery.dataTables.min.js"></script>
	
    <script type="text/javascript" src="bootstrap-dist/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="bootstrap-dist/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="bootstrap-dist/js/dataTables.editor.min.js"></script>
    <script src="bootstrap-dist/js/editor.js"></script>
   
    <script src="https://kit.fontawesome.com/f1dc0c95ad.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.js"></script>


	
</body>
</html>