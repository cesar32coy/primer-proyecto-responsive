

<!DOCTYPE html>
<html lang="en" >
<head >
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="bootstrap-dist/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="bootstrap-dist/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="bootstrap-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-dist/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="bootstrap-dist/css/fixedColumns.dataTables.min.css">
    <link rel="stylesheet" href="bootstrap-dist/css/style.css">


  <title>Tabla Responsive</title>
</head>
<body >


    <h1 class="p-3  bg-primary text-white text-center">Data Table Responsive y Ajax</h1>
     
    <span class="text-center" id="resultadoelim"></span>
    <span class="text-center" id="resultadoedit"></span>
    <span class="text-center" id="resultado2"></span>
    <div id="preload" class="text-center"></div>
    
    
    
    <div  class="container" >
      
        <button type="button" class="btn btn-primary btninter right" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Nuevo <i class="fas fa-plus"></i></button><br />


  <table id="example" class="display responsive nowrap compact stripe" style="width:100%">
        <thead>
            <tr>
                <th>id</th>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Cumpleaños</th>
                <th>Direccion</th>
                <th>Ciudad</th>
                <th>Accion</th>
               

            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>id</th>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Cumpleaños</th>
                <th>Direccion</th>
                <th>Ciudad</th>
                <th>Accion</th>
               
            </tr>
        </tfoot>

    </table>
</div>

                <!--======= inicio Modal para registro========-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary text-white ">
        <h5 class="modal-title" id="exampleModalLabel">Registro</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" id="frm">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Nombre: <strong class="text-danger" style="cursor: pointer;" title="campo obligatorio">*</strong></label>
            <input type="text" class="form-control" id="nombre" autocomplete="off">
            <label id="lbNombre"></label>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Apellidos: <strong class="text-danger" style="cursor: pointer;" title="campo obligatorio">*</strong></label>
            <input type="text" class="form-control" id="apellidos" autocomplete="off">
            <label id="lbApellidos"></label>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Nacimiento: <strong class="text-danger" style="cursor: pointer;" title="campo obligatorio">*</strong></label>
            <input type="date" class="form-control" id="fecha">
            <label id="lbFecha"></label>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Dirección: <strong class="text-danger" style="cursor: pointer;" title="campo obligatorio">*</strong></label>
            <input type="text" class="form-control" id="direccion" autocomplete="off">
            <label id="lbDireccion"></label>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Ciudad:</label>
            <input type="text" class="form-control" id="ciudad" autocomplete="off">
            <label id="lbCiudad"></label>
          </div>
          
          <button type="submit" class="btn btn-primary" id="cargar">Guardar <i class="far fa-save"></i></button>
          
        </form>
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-danger" data-dismiss="modal" id="salir">Salir</button>

      </div>
    </div>
  </div>
  </div>
   <!--======= Fin Modal para registro========-->

   <!--======= Inicio Modal para editar========-->

  <div class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-warning text-white">
        <h5 class="modal-title" id="exampleModalLabel">Editar Registro</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" id="frmedit">
            <div class="form-group">
            <label for="recipient-name" class="col-form-label"></label>
            <input type="hidden" class="form-control" id="id">
            <label id="lbNombre"></label>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Nombre:</label>
            <input type="text" class="form-control" id="nombre2">
            <label id="lbNombre"></label>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Apellidos:</label>
            <input type="text" class="form-control" id="apellidos2">
            <label id="lbApellidos"></label>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Nacimiento:</label>
            <input type="text" class="form-control" id="fecha2">
            <label id="lbFecha"></label>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Dirección:</label>
            <input type="text" class="form-control" id="direccion2">
            <label id="lbDireccion"></label>
          </div>

          <button type="submit" class="btn btn-warning" >Editar<i class='far fa-edit'></i></button>
        </form>
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>

      </div>
    </div>
  </div>
</div>

<!--======= Fin Modal para editar========-->

<!--======= Inicio Modal eliminar========-->

<div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger text-white">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar Registro</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" id="frmeliminar">
            <div class="form-group">
            <label for="recipient-name" class="col-form-label"></label>
            <input type="text" class="form-control" id="ids">

            <label id="lbid">¿Esta Seguro que desea Eliminar</label>
          </div>

          <button type="submit" class="btn btn-danger" >Eliminar<i class='far fa-edit'></i></button>
        </form>
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>

      </div>
    </div>
  </div>
</div>
<!--======= Inicio Modal eliminar========-->



  <script type="text/javascript" src="bootstrap-dist/js/jquery-3.3.1.js"></script>

  <script type="text/javascript" src="bootstrap-dist/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="bootstrap-dist/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="bootstrap-dist/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="bootstrap-dist/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="bootstrap-dist/js/dataTables.fixedColumns.min.js"></script>
    <script src="bootstrap-dist/js/json.js"></script>
    <script type="text/javascript" src="bootstrap-dist/js/bootstrap.min.js"></script>

    <script src="https://kit.fontawesome.com/f1dc0c95ad.js" crossorigin="anonymous"></script>






</body>
</html>
