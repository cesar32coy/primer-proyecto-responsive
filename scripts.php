<script src="bootstrap-dist/js/jquery-3.3.1.js"></script>

  
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
   <script type="text/javascript" src="bootstrap-dist/js/jquery.dataTables.js"></script>
   <script type="text/javascript" src="bootstrap-dist/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript" src="js/password_strength.js"></script>
  <script type="text/javascript" src="js/jquery-strength.js"></script>
  <script type="text/javascript" src="js/jquery.validate.js" ></script>
  
    
	
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>

	 
	 <script>
	 	
$('.modal').modal({
      dismissible: false
     });
     $(".button-collapse").sideNav();
     
    $("#mostrar").click(function(){
      $('#mdcrear').modal('open');;
      });
    $('select').material_select();
      

    jQuery(function($) {
             
            $(".check-seguridad").strength({
                templates: {
                toggle: '<span class="input-group-addon"><span class="glyphicon glyphicon-eye-open {toggleClass}"></span></span>'
                 
                },
                scoreLables: {
                        empty: 'Vacío',
                        invalid: 'Invalido',
                        weak: 'Débil',
                        good: 'Bueno',
                        strong: 'Fuerte'
                    }, 
                scoreClasses: {
                        empty: '',
                        invalid: 'label-danger',
                        weak: 'label-warning',
                        good: 'label-info',
                        strong: 'label-success'
                    },
 
            });
        });

    </script>